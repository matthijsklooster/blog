<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
\Illuminate\Support\Facades\Broadcast::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'web'], function () {
    Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
        Route::get('create', ['uses' => 'BlogController@create', 'as' => 'create']);
        Route::post('store', ['uses' => 'BlogController@store', 'as' => 'store']);
        Route::get('{blog}', ['uses' => 'BlogController@show', 'as' => 'show']);
        Route::post('{blog}/comment', ['uses' => 'BlogController@comment', 'as' => 'comment']);
    });
});
