<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $guarded = [];

    protected $dates = [
        'created_at'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function getBody() {
        return $this->body;
    }

    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function attachComment($data) {
        return $this->comments()->create([
            'body' => $data->body,
            'user_id' => auth()->id()
        ]);
    }
}
