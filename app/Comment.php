<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    public function setUserId($id) {
        $this->user_id = $id;
        return $this;
    }

    public function setBlogId($id) {
        $this->blog_id = $id;
        return $this;
    }

    public function getBody() {
        return $this->body;
    }
}
