<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Events\NewComment;
use App\Http\Requests\CreateBlogRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{
    public function show(Blog $blog) {
        $comments = $blog->comments()->with('user')->get();
        return view('blogs.show', compact('blog', 'comments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('blogs.create');
    }

    /**
     * @param CreateBlogRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateBlogRequest $request) {
        // Kijk in de request of de blog is aangemaakt, zo ja: notification
        if ($request->isValid())
            return redirect()->route('home')->with('notification', ['type' => 'success', 'message' => "Succesvol een blog aangemaakt"]);

        return redirect()->route('home')->with('notification', ['type' => 'danger', 'message' => "Succesvol geen blog aangemaakt"]);
    }

    /**
     * @param Request $request
     * @param Blog $blog
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request, Blog $blog) {

        $comment = new Comment;
        $comment
            ->setBody($request->body)
            ->setUserId(auth()->id())
            ->setBlogId($blog->id)
            ->save();

        // Manier 2:
        // Via de model een nieuwe comment aan laten maken, zodat we de controller alleen gebruiken voor het afhandelen
        // waar het oorspronkelijk voor gemaakt is in de MVC structuur.

//        $comment = $blog->attachComment((object)$request->all());

        event(new NewComment($comment));

        if (!$comment){
            return redirect()->back()->with('notification', ['type' => 'danger', 'message' => "Succesvol geen comment aangemaakt"]);
        }

        return redirect()->back()->with('notification', ['type' => 'success', 'message' => "Succesvol een comment aangemaakt"]);
    }
}
