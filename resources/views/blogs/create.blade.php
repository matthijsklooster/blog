@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('blog.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="title">Titel</label>
                                <input type="text" name="title" placeholder="Titel" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="body">Body</label>
                                <textarea class="form-control" name="body" id="" cols="30" rows="10"></textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Maak aan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
