@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $blog->getTitle() }}
                    </div>

                    <div class="card-body">
                        {!!  $blog->getBody() !!}
                    </div>

                    <div class="card-footer">
                        Aangemaakt op: {{ $blog->getCreatedAt()->format('Y-m-d') }}
                    </div>
                </div>

                <div class="card mt-4">
                    <div class="card-header">Comments</div>
                    <div class="card-body">
                        <form action="{{ route('blog.comment', ['blog' => $blog]) }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="comment">Bericht</label>
                                <textarea class="form-control" id="comment" name="body" cols="30" rows="5"></textarea>
                            </div>
                            <button class="btn btn-primary">Maak bericht</button>
                        </form>

                        <hr>

                        <div>
                            <comment-component :blog="{{ $blog }}" :comments="{{ $comments }}"></comment-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
