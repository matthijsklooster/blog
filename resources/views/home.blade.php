@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Alle blogs</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>Titel</th>
                                <th>User</th>
                                <th>Aangemaakt op</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($blogs as $blog)
                                <tr>
                                    <td>
                                        <a href="{{ route('blog.show', ['blog' => $blog]) }}">{{ $blog->getTitle() }}</a>
                                    </td>
                                    <td>{{ $blog->user->getName() }}</td>
                                    <td>{{ $blog->getCreatedAt()->format('Y-m-d') }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-danger">Verwijder</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
